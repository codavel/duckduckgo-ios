### Codavel SDK ###

The purpose of this project is to demonstrate how easy is to integrate Codavel SDK into an iOS app, by modifying the iOS App for DuckDuckGo, an open-source search engine emphasizing searchers' privacy.

You can check the original README [here](README.original.md), or directly in the original repository [here](https://github.com/duckduckgo/IOS).


### Integration ###

In order to integrate Codavel SDK, the following modifications to the original project were required (you can check all the code changes [here](https://gitlab.com/codavel/duckduckgo-ios/-/commit/9dfbe9a14770f62ff3aa97ad0cf2192f12b7ac1c)):

1. Include Codavel's dependency into the applications' Podfile, required to download and use Codavel's SDK
2. Start Codavel's Service with Codavel's Application ID and Secret when the app starts, by changing the didFinishLaunchingWithOptions method in the AppDelegate.
3. Register our HTTP interceptor into the application's URLSessionConfiguration, so that all the HTTP requests executed through the app's are processed and forwarded to our SDK.
